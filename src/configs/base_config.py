"""
Базовый конфигурационный файл.
Предлагается использовать YACS config (https://github.com/rbgirshick/yacs) для
поддержки базового конфигурационного файла. Затем для каждого конкретного
эксперимента можно создать конфигурационный файл, который будет переопределять
необходимые параметры базового конфигурационного файла.


Пример использования:
    1. Создать конфигурационный файл для конкретного эксперимента
    (например, configs/experiment_1.yaml)
    2. B конфигурационном файле переопределить необходимые параметры
    базового конфигурационного файла
    3. B модуле, где необходимо использовать конфигурационные параметры,
    импортировать функцию combine_config
    4. Вызвать функцию combine_config, передав в качестве аргумента путь
    к конфигурационному файлу конкретного эксперимента
    5. Полученный объект yacs CfgNode можно использовать для доступа
    к конфигурационным параметрам
"""

import os.path as osp
from datetime import datetime
from typing import Optional, Union

from yacs.config import CfgNode as CN


def get_cfg_defaults() -> CN:
    """Возвращает yacs CfgNode объект co значениями по умолчанию"""
    _C = CN()

    # Root directory of project
    _C.ROOT = CN()
    _C.ROOT.PATH = ''  # Путь к корневой директории проекта в Google Colab

    # Dataset
    _C.DATASET = CN()

    # Raw image dir, interim (after split) image dir
    _C.DATASET.RAW_IMG_DIR = 'data/raw/sfd_with_labels'
    _C.DATASET.INTERIM_IMG_DIR = 'data/interim/dataset_split'
    _C.DATASET.TRAIN_DIR = 'train'
    _C.DATASET.VAL_DIR = 'val'
    _C.DATASET.TEST_DIR = 'test'

    # Proportion for split_dataset()
    _C.DATASET.TRAIN_SIZE = 0.6
    _C.DATASET.TEST_SIZE = 0.2
    _C.DATASET.VAL_SIZE = 0.2
    _C.DATASET.CLASSES = ['safe driving',
                        'texting - right',
                        'talking on the phone - right',
                        'texting - left',
                        'talking on the phone - left',
                        'operating the radio',
                        'drinking',
                        'reaching behind',
                        'hair and makeup',
                        'talking to passenger']


    # datamodule params
    _C.DATAMODULE = CN()
    _C.DATAMODULE.IMG_SIZE = 224
    _C.DATAMODULE.BATCH_SIZE = 1
    _C.DATAMODULE.NUM_WORKERS = 2
    _C.DATAMODULE.SHUFFLE_TRAIN = True
    _C.DATAMODULE.SHUFFLE_VAL = False
    _C.DATAMODULE.SHUFFLE_TEST = False

    # Learning
    _C.LEARNING = CN()
    _C.LEARNING.BASE_MODEL = 'efficientnet-b0'
    _C.LEARNING.CHECKPOINT_PATH: Optional[str] = None
    _C.LEARNING.ETA = 3e-2
    _C.LEARNING.MAX_EPOCHS = 5
    _C.LEARNING.DEVICE = 'cpu'
    _C.LEARNING.SEED = 42

    #_C.LEARNING.CHECKPOINT_PATH: Optional[str] = None  # 'models/efficientnet-b0.ckpt'

    # MLFLOW
    _C.MLFLOW = CN()
    _C.MLFLOW.EXPERIMENT = ''
    _C.MLFLOW.LOG_CHECKPOINTS = False
    _C.MLFLOW.LOG_MODEL = False
    _C.MLFLOW.RUN_NAME = f'{datetime.now().strftime("%yy_%mm_%dd_%Hh_%Mm_%Ss")}'

    # Local logging
    _C.LOGGING = CN()
    _C.LOGGING.LOGS_FOLDER = 'logs'
    _C.LOGGING.LOGGING_INTERVAL = 'step'

    # Checkpoint
    _C.CHECKPOINT = CN()
    _C.CHECKPOINT.CKPT_FOLDER = 'checkpoints/'
    _C.CHECKPOINT.FILENAME = '{epoch}_{valid_acc:.2f}_{valid_loss:.2f}'
    _C.CHECKPOINT.SAVE_TOP_K = 1
    _C.CHECKPOINT.CKPT_MONITOR = 'valid_loss'
    _C.CHECKPOINT.CKPT_MODE = 'min'

    # Early stopping
    _C.ES = CN()
    _C.ES.MONITOR = 'valid_loss'
    _C.ES.MIN_DELTA = 2e-4
    _C.ES.PATIENCE = 10
    _C.ES.VERBOSE = False,
    _C.ES.MODE = 'min'

    _C.CFG_INFO = CN()
    _C.CFG_INFO.CFG_PATH = ''
    return _C.clone()

def combine_config(cfg_path: Union[str, None] = None):
    """
    Объединяет базовый конфигурационный файл c
    конфигурационным файлом конкретного эксперимента
    Args:
         cfg_path (str): file in .yaml or .yml format with
         config parameters or None to use Base config
    Returns:
        yacs CfgNode object
    """
    base_config = get_cfg_defaults()
    if cfg_path is not None:
        if osp.exists(cfg_path):
            base_config.merge_from_file(cfg_path)
        else:
            raise FileNotFoundError(f'File {cfg_path} does not exists')

    # Join paths
    base_config.DATASET.RAW_IMG_DIR = osp.join(
        base_config.ROOT.PATH,
        base_config.DATASET.RAW_IMG_DIR
    )
    base_config.DATASET.INTERIM_IMG_DIR = osp.join(
        base_config.ROOT.PATH,
        base_config.DATASET.INTERIM_IMG_DIR
    )
    base_config.DATASET.TRAIN_DIR = osp.join(
        base_config.DATASET.INTERIM_IMG_DIR,
        base_config.DATASET.TRAIN_DIR
    )
    base_config.DATASET.VAL_DIR = osp.join(
        base_config.DATASET.INTERIM_IMG_DIR,
        base_config.DATASET.VAL_DIR
    )
    base_config.DATASET.TEST_DIR = osp.join(
        base_config.DATASET.INTERIM_IMG_DIR,
        base_config.DATASET.TEST_DIR
    )
    # путь к чекпоинту модели, c которого хотим продолжить обучение
    if base_config.LEARNING.CHECKPOINT_PATH:
        base_config.LEARNING.CHECKPOINT_PATH = osp.join(
            base_config.ROOT.PATH,
            base_config.LEARNING.CHECKPOINT_PATH
        )

    # локальное логгирование аналогично mlflow
    # путь к эксперименту
    # ROOT.PATH = 'attention_control/'
    # LOGGING.LOGS_FOLDER = 'logs/'
    # MLFLOW.EXPERIMENT = 'base'
    # LOGGING.EXPERIMENT_PATH = 'attention_control/logs/base/'
    base_config.LOGGING.EXPERIMENT_PATH = osp.join(
        base_config.ROOT.PATH,
        base_config.LOGGING.LOGS_FOLDER,
        base_config.MLFLOW.EXPERIMENT
    )

    # LEARNING.BASE_MODEL='efficientnet/b0'
    # LEARNING.RUN_NAME='23y_07m'
    # MLFLOW.RUN_NAME='efficientnet_b0_23y_07m'
    base_config.MLFLOW.RUN_NAME = base_config.LEARNING.BASE_MODEL.replace('/', '_') \
                                  + "_" + base_config.MLFLOW.RUN_NAME

    # путь к запуску внутри эксперимента
    # LOGGING.EXPERIMENT_PATH = 'attention_control/logs/base/'
    # MLFLOW.RUN_NAME='efficientnet_b0_23y_07m'
    # LOGGING.RUN_PATH='attention_control/logs/base/efficientnet_b0_23y_07m'
    base_config.LOGGING.RUN_PATH = osp.join(
        base_config.LOGGING.EXPERIMENT_PATH,
        base_config.MLFLOW.RUN_NAME
    )
    # путь к сохраненной модели внутри запуска
    # LOGGING.SAVING_MODEL_PATH='attention_control/logs/base/efficientnet_b0_23y_07m'
    base_config.LOGGING.SAVING_MODEL_PATH = osp.join(
        base_config.LOGGING.RUN_PATH,
        base_config.LEARNING.BASE_MODEL.replace('/', '_') + '.pt'
    )
    # путь к чекпоинтам модели внутри запуска
    base_config.CHECKPOINT.CKPT_PATH = osp.join(
        base_config.LOGGING.RUN_PATH,
        base_config.CHECKPOINT.CKPT_FOLDER
    )

    base_config.CFG_INFO.CFG_PATH = osp.join(
        base_config.ROOT.PATH,
        base_config.CFG_INFO.CFG_PATH
    )

    return base_config

import io
import os

import gradio as gr
from dotenv import load_dotenv
from fastapi import FastAPI, File, HTTPException, UploadFile
from PIL import Image

from src.models.BaseClassifier import BaseClassifierProd

LOCALPATH_ART = '/app//src/configs'
EXAMPLE_FOLDER = '/app/examples'
OVERWRITE = True

def model_init():
    model_name = os.getenv('MODEL_NAME')
    stage = os.getenv('MODEL_STAGE')
    if model_name is None or stage is None:
        raise Exception("Missing MODEL_NAME or MODEL_STAGE env variable. Add to .env")

    model = BaseClassifierProd(model_name=model_name,
                        stage=stage,
                        localpath_art=LOCALPATH_ART,
                        overwrite=OVERWRITE)
    return model

load_dotenv()
app = FastAPI()
model = model_init()

@app.post("/invocations")
async def get_predict(file: UploadFile = File(...)):
    if file.filename.endswith(".jpg"):
        file = await file.read()
        file = Image.open(io.BytesIO(file))
        return model.predict(file)[0]
    else:
        raise HTTPException(status_code=400,
                            detail="Invalid file format. Only JPG Files accepted.")

def get_predict_gradio(file):
   return model.predict(file)[1]

def get_example_paths(folder_path):
    file_list = os.listdir(folder_path)
    file_paths = [[os.path.join(folder_path, filename)] for filename in file_list]

    return file_paths

demo = gr.Interface(fn=get_predict_gradio,
             inputs=gr.Image(type="pil"),
             outputs=gr.Label(num_top_classes=5),
             allow_flagging = "never",
             examples=get_example_paths(EXAMPLE_FOLDER),
            )

CUSTOM_PATH = "/demo"
app = gr.mount_gradio_app(app, demo, path=CUSTOM_PATH)

if __name__ == "__main__":
    get_predict()

import os
from pathlib import Path

import mlflow
import pytorch_lightning as pl
import torch
from torch import nn

from src.configs.augmentations_config import get_torchvision_transforms
from src.configs.base_config import combine_config


class BaseClassifier(pl.LightningModule):
    def __init__(self, **kwargs) -> None:
        """Бейзлайн модель EfficientNet, предобученная
         и c базовой архитектурой. Класс отнаследован
         от pl.LightningModule
        """
        super().__init__()

    def forward(self, x: torch.Tensor = None) -> torch.Tensor:
        """Функция для forward pass модели

        Args:
            x (torch.Tensor, required): Тензор c изображениями
            для forward pass. По умолчанию None.

        Returns:
            torch.Tensor: Возвращает тензор c результатом прохода модели.
        """
        return self.model(x)

class BaseClassifierProd():
    def __init__(self,
                 model_name: str,
                 stage: str = 'Production',
                 localpath_art: str = '/root/app/artifacts',
                 overwrite: bool = False) -> None:
        self.model_name = model_name
        self.stage = stage
        self.localpath_art = Path(localpath_art)
        self.overwrite = overwrite

        self.device = os.getenv('DEVICE_INFERENCE_MODEL')
        if self.device is None:
            self.device = 'cpu'
        print(f"Device is '{self.device}'")
        self.setup()

    def setup(self) -> None:
        """
        Performs the setup steps for the classifier.
        - Downloads the required artifacts including model weightd
        and configuration file.
        """
        self.download_artifacts()

        self.model = torch.load(self.localpath_art / 'model/data/model.pth',
                                map_location = self.device)
        self.cfg = combine_config(self.localpath_art / 'params.yaml')

    def download_artifacts(self) -> None:
        """
        Downloads the required artifacts from the source.
        - Skips downloading if an artifact already exists and overwrite=False.
        """
        source = self.get_source_artifacts()

        artifacts = [
            'model/data/model.pth',
            #'params.yaml'
        ]

        os.makedirs(self.localpath_art / 'model/data/', exist_ok=True)
        for artifact in artifacts:
            artifact_uri = source + artifact
            local_path_artifact = self.localpath_art / artifact

            if not self.overwrite and (local_path_artifact).exists():
            # Skips downloading if an artifact already exists and overwrite=False.
                continue

            mlflow.artifacts.download_artifacts(artifact_uri=artifact_uri,
                                                dst_path=local_path_artifact.parent)

    def get_source_artifacts(self) -> str:
        """
        Retrieves the source of the artifacts for the specified model and stage.

        Returns:
            str: The source URL of the artifacts.

        Raises:
            Exception: If the specified model and stage are not found.
        """
        client = mlflow.MlflowClient()
        for mv in client.search_model_versions(f"name='{self.model_name}'"):
            dict_mv = dict(mv)
            if dict_mv['current_stage'] == self.stage:
                return dict_mv['source'].split('model')[0]
        raise Exception(f"Model '{self.model_name}:{self.stage}' not found")

    def predict(self, file):
        """Функция, выполняющая предсказание модели на одном изображении.

        Для предсказания используется:
        - модель из src/models, название модели указывается в аргументе --model
        - изображение находящееся по пути из аргумента --input_path

        Результат модели сохраняется по пути из аргумента --output_path

        Args:
            args (argparse.Namespace): Аргументы, указанные при запуске скрипта
            idx_to_classes (list[str]): Лист c названиями классов
        """

        image_size = self.cfg.DATAMODULE.IMG_SIZE
        target_image = file

        transform_test = get_torchvision_transforms(image_size)
        input = transform_test(target_image)

        self.model.eval()
        with torch.inference_mode():
            outputs = self.model(input.unsqueeze(0).to(self.device)).cpu()
            probabilities = nn.functional.softmax(outputs, dim=1).detach()
        target_image_pred_label = torch.argmax(outputs, dim=1).item()

        idx2label = self.cfg.DATASET.CLASSES
        predict_class = idx2label[target_image_pred_label]

        proba_rounded_values = [round(float(value), 2) for value in probabilities[0]]
        proba_dict = {k: v for k, v in zip(idx2label, proba_rounded_values)}

        return predict_class, proba_dict
